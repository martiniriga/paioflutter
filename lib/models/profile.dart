import 'package:flutter/foundation.dart';
import 'dart:convert';
import '../webservice.dart';

class Profile {
  final String name;
  final String email;
  String phone;
  String gender;
  String avatarUrl;
  String countyName;
  String industryName;

  Profile(
      {@required this.name,
      @required this.email,
      this.phone,
      this.gender,
      this.avatarUrl,
      this.countyName,
      this.industryName});

  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
        name: json['name'],
        email: json['email'],
        phone: json['phone'],
        gender: json['gender'],
        avatarUrl: json['avatar_url'],
        countyName: json['countyName'],
        industryName: json['industryName']);
  }

  static Resource<Profile> get data {
    return Resource(
        url: 'https://paio.app/api/profile',
        parse: (response) {
          final result = json.decode(response.body);
          Profile profile = result['data'];
          return profile;
        }
    );

  }
}
