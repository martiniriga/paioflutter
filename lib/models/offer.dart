import 'package:flutter/material.dart';

class Offer {
  final int id;
  final String name;
  final int supplierId;
  final int categoryId;
  final String currency;
  final String description;
  final double price;
  final String brand;
  final String excerpt;
  final String avatarUrl;
  final List<String> tagNames;
  bool hasFavorited;
  bool hasCoupon;
  final String link;
  final String supplier;
  final Media media;
  final int rating;
  final Category category;

  Offer(
      {this.id,
      this.name,
      this.excerpt,
      this.brand,
      this.description,
      this.avatarUrl,
      this.tagNames,
      this.price,
      this.currency,
      this.link,
      this.categoryId,
      this.supplierId,
      this.supplier,
      this.media,
      this.hasFavorited,
      this.hasCoupon,
      this.rating,
      this.category});

  factory Offer.fromJson(Map<String, dynamic> json) {
    return Offer(
      id: json['id'],
      name: json['name'],
      currency: json['currency'],
      description: json['description'],
      price: json['published_date'],
      supplierId: json['supplier_id'],
      categoryId: json['category_id'],
      excerpt: json['excerpt'],
      avatarUrl: json['cover_image_url'],
      tagNames: json['tag_names'],
      hasFavorited: json['hasFavorited'],
      hasCoupon: json['hasCoupon'],
      brand: json['brand'],
      link: json['link'],
      media: json['media'],
      supplier: json['supplier']['name'],
      rating: json['rating'],
      category: json['category']
    );
  }
}

class Media {
  final String url;

  Media({this.url});

  factory Media.fromJson(Map<String, dynamic> json) {
    return Media(url: json['url']);
  }
}

class Category {
  final int id;
  final String name;

  Category({this.id, this.name});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json['id'],
      name: json['name'],
    );
  }
}


