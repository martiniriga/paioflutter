import 'package:flutter/material.dart';

class Event {
  final int categoryId;
  final String avatarUrl;
  final String description;
  final String endDate;
  final String endTime;
  final String excerpt;
  final bool featured;
  bool hasFavorited;
  bool hasRsvp;
  final int id;
  final double lat;
  final double lng;
  final String location;
//  final Rsvp myRsvp;
  final String name;
  final String startDate;
  final String startTime;
  final int supplierId;
  final String link;

  Event(
      {this.id,
      this.name,
      this.excerpt,
      this.description,
      this.avatarUrl,
      this.startDate,
      this.startTime,
      this.endDate,
      this.endTime,
      this.lat,
      this.lng,
      this.location,
      this.link,
      this.categoryId,
//      this.myRsvp,
      this.supplierId,
      this.featured,
      this.hasFavorited,
      this.hasRsvp});

  factory Event.fromJson(Map<String, dynamic> json) {
    return Event(
        id: json['id'],
        name: json['name'],
        excerpt: json['excerpt'],
        description: json['description'],
        avatarUrl: json['cover_image_url'],
        startDate: json['start_date'],
        startTime: json['start_time'],
        endDate: json['end_date'],
        endTime: json['end_time'],
        lat: json['lat'],
        lng: json['lng'],
        location: json['location'],
        link: json['link'],
        categoryId: json['category_id'],
//        myRsvp: json['myRsvp'],
        supplierId: json['supplier_id'],
        featured: json['featured'],
        hasFavorited: json['hasFavorited'],
        hasRsvp: json['hasRsvp']);
  }
}

class Rsvp {
  final String code;
  final int eventId;
  final int id;
  final String rsvp;
  final String status;

  Rsvp({this.id, this.rsvp, this.status, this.code, this.eventId});

  factory Rsvp.fromJson(Map<String, dynamic> json) {
    return Rsvp(
        id: json['id'],
        eventId: json['event_id'],
        code: json['code'],
        rsvp: json['rsvp'],
        status: json['status']);
  }
}
