import 'package:flutter/material.dart';

class Blog {
  final int id;
  final String title;
  final String author;
  final String content;
  final String publishedDate;
  final int categoryId;
  final int supplierId;
  final String excerpt;
  final String avatarUrl;
  final List<String> tagNames;
  bool hasFavorited;
  bool hasBookmarked;
  final String link;
  final Media media;


  Blog(
      {this.id,
      this.title,
      this.excerpt,
      this.content,
      this.avatarUrl,
      this.tagNames,
      this.publishedDate,
      this.author,
      this.link,
      this.categoryId,
      this.supplierId,
      this.media,
      this.hasFavorited,
      this.hasBookmarked});

  factory Blog.fromJson(Map<String, dynamic> json) {
    return Blog(
        id: json['id'],
        title: json['title'],
        author: json['author'],
        content: json['content'],
        publishedDate: json['published_date'],
        supplierId: json['supplier_id'],
        categoryId: json['category_id'],
        excerpt: json['excerpt'],
        avatarUrl: json['cover_image_url'],
        tagNames: json['tag_names'],
        hasFavorited: json['hasFavorited'],
        hasBookmarked: json['hasBookmarked'],
        link: json['link'],
        media: json['media']);
  }
}

class Media {
  final String url;

  Media({this.url});

  factory Media.fromJson(Map<String, dynamic> json) {
    return Media(url: json['url']);
  }
}
