import 'package:flutter/foundation.dart';
import 'dart:convert';
import '../webservice.dart';

class County {
  final int id;
  final String name;

  County({@required this.id, @required this.name});

  factory County.fromJson(Map<String,dynamic> json) {
    return County(
        id: json['id'],
        name: json['name']);
  }

  static Resource<List<County>> get all {

    return Resource(
        url: 'https://paio.app/api/counties',
        parse: (response) {
          final result = json.decode(response.body);
          Iterable list = result['data'];
          return list.map((model) => County.fromJson(model)).toList();
        }
    );

  }
}
