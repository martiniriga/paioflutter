import 'package:flutter/material.dart';
import '../models/Profile.dart';
import '../widgets/SubmitButton.dart';
import '../config/Palette.dart';
import '../webservice.dart';

class ProfilePage extends StatefulWidget {
  static const String routeName = "/profile";

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var _isLoading = false;
  Profile _profile;

  @override
  void initState() {
    _populateProfile();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("My Account")),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CircleAvatar(
                      radius: 32,
                      backgroundImage: NetworkImage(
                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMdhHG0CPtHLwM0UnTh4q3HHVrcSVI-suO4x5F1xSHsESuVrmx")),
                  RaisedButton(
                      child: Text("Interests"),
                      onPressed: viewInterests,
                      color: Palette.accentColor,
                      textColor: Palette.primaryTextColorLight,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0))),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(16),
                    child: Container(
                      color: Palette.grey6,
                      child: IconButton(
                          icon: Icon(Icons.edit, color: Colors.white)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 24),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Name"),
                    Text(_profile.name),
                  ],
                ),
              ),
              Divider(
                color: Palette.grey6,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Gender"),
                    Text(_profile.gender),
                  ],
                ),
              ),
              Divider(
                color: Palette.grey6,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Location"),
                    Text(_profile.countyName),
                  ],
                ),
              ),
              Divider(
                color: Palette.grey6,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Email"),
                    Text(_profile.email),
                  ],
                ),
              ),
              Divider(color: Palette.grey6),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Phone"),
                    Text(_profile.phone),
                  ],
                ),
              ),
              Divider(color: Palette.grey6),
              GestureDetector(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                    width: double.infinity,
                    height: 32,
                    child: Text(
                      "Sign out",
                      style: TextStyle(color: Colors.redAccent),
                    )),
              )),
              Row(
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                        child: Text("My Events"),
                        onPressed: viewInterests,
                        color: Colors.redAccent,
                        textColor: Palette.primaryTextColorLight,
                        padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8))),
                  ),
                  SizedBox(width: 16),
                  Expanded(
                      child: SubmitButton(
                          buttonText: "My Coupons", selectHandler: viewCoupons)
                      ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void viewInterests() {}

  viewCoupons() {}

  void _populateProfile() {
    setState(() {
      _isLoading = true;
    });
    Webservice().load(Profile.data).then((p) {
      setState(() => {
        _profile = p
      });
      setState(() {
        _isLoading = false;
      });
    });
  }
}
