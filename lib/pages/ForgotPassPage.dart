import 'package:flutter/material.dart';
import '../config/Palette.dart';
import '../config/Styles.dart';

class ForgotPassPage extends StatefulWidget {
  static const String routeName ="/forgot-password";
  @override
  _ForgotPassPageState createState() => _ForgotPassPageState();
}

class _ForgotPassPageState extends State<ForgotPassPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(title: Text("Forgot Password")),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                  child: Text("Need help with your Password?",
                      textAlign: TextAlign.center, style: Styles.textHeading)),
            ),
            Padding(
                padding: const EdgeInsets.all(16.0),
                child: Center(
                    child: Text(
                        "Enter the email you used to register and we'll help you reset your password",
                        textAlign: TextAlign.center))),
            resetForm()
          ],
        ),
      ),
    );
  }

  resetForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: const InputDecoration(
                hintText: 'Enter your email',
                labelText: 'Email',
              ),
              validator: (value) {
                bool emailValid =
                    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        .hasMatch(value);
                if (value.isEmpty) {
                  return 'Please enter email';
                } else if (!emailValid) {
                  return 'Please enter a valid email';
                }
                return null;
              },
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(16.0),
              child: SizedBox(
                width: double.infinity,
                child: RaisedButton(
                    padding: EdgeInsets.all(16),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        // Process data.
                      }
                    },
                    color: Colors.redAccent,
                    textColor: Palette.primaryTextColorLight,
                    child: Text('Reset Password')),
              )),
        ],
      ),
    );
  }
}
