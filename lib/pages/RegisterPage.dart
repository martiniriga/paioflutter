import 'package:flutter/material.dart';
import '../models/County.dart';
import 'package:provider/provider.dart';
import '../config/Palette.dart';
import '../models/Industry.dart';
import '../models/Register.dart';
import '../providers/industries.dart';
import '../webservice.dart';

class RegisterPage extends StatefulWidget {
  static const String routeName = "/register";

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String _genderError;
  String _industryError;
  String _countyError;
  final _formKey = GlobalKey<FormState>();
  final _register = Register();
  List<Industry> _industries = [];
  List<County> _counties = [];
  var _isInit = true;
  var _isLoading = false;


  @override
  void initState() {
    _populateCounties();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Industries>(context).fetchIndustries().then((_){
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  void _populateCounties() {
    setState(() {
      _isLoading = true;
    });
    Webservice().load(County.all).then((counties) {
      setState(() => {
        _counties = counties
      });
      setState(() {
        _isLoading = false;
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    _industries = Provider.of<Industries>(context).items;
    return Scaffold(
        appBar: AppBar(title: Text("Create Account")),
        body: _isLoading
            ? Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Center(child: CircularProgressIndicator()),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text("Loading ..."),
                    ),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[registerForm()]),
                )));
  }

  registerForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
              hintText: 'Enter fullname',
              labelText: 'FullName',
            ),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter your fullname';
              }
              return null;
            },
            onSaved: (val) => setState(() => _register.name = val),
          ),
          TextFormField(
            decoration: const InputDecoration(
              hintText: 'Enter your email',
              labelText: 'Email',
            ),
            validator: (value) {
              bool emailValid =
                  RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                      .hasMatch(value);
              if (value.isEmpty) {
                return 'Please enter email';
              } else if (!emailValid) {
                return 'Please enter a valid email';
              }
              return null;
            },
            onSaved: (val) => setState(() => _register.email = val),
          ),
          Padding(
            padding: const EdgeInsets.only(top:3.0),
            child: Text("Gender"),
          ),
          genderDropDown(),
          _genderError == null
              ? SizedBox.shrink()
              : Text(
                  _genderError ?? "",
                  style: TextStyle(color: Colors.red),
                ),
          Padding(
            padding: const EdgeInsets.only(top:3.0),
            child: Text("County"),
          ),
          countyDropDown(),
          _countyError == null
              ? SizedBox.shrink()
              : Text(
                  _countyError ?? "",
                  style: TextStyle(color: Colors.red),
                ),
          Padding(
            padding: const EdgeInsets.only(top:3.0),
            child: Text("Industry"),
          ),
          industryDropDown(),
          _industryError == null
              ? SizedBox.shrink()
              : Text(
                  _industryError ?? "",
                  style: TextStyle(color: Colors.red),
                ),
          TextFormField(
            decoration: const InputDecoration(
              hintText: '0712******',
              labelText: 'Phone',
            ),
            keyboardType: TextInputType.numberWithOptions(),
            maxLength: 10,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter phone number';
              } else if (value.length != 10) {
                return 'Please enter a valid phone number';
              }
              return null;
            },
            onSaved: (val) => setState(() => _register.phone = val),
          ),
          TextFormField(
            decoration: const InputDecoration(
              hintText: 'Enter your password',
              labelText: 'Password',
            ),
            obscureText: true,
            autocorrect: false,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              } else if (value.length < 6) {
                return 'Password must be at least 6 characters';
              }
              return null;
            },
            onSaved: (val) => setState(() => _register.password = val),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  color: Colors.blue,
                  textColor: Palette.primaryTextColorLight,
                  padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
                  onPressed: () => _validateForm(),
                  child: Text('Continue'),
                ),
              )),
        ],
      ),
    );
  }

  genderDropDown() {
    return DropdownButton<String>(
      hint: Text('Select Gender'),
      value: _register.gender,
//      icon: Icon(Icons.arrow_downward),
//      iconSize: 24,
//      elevation: 16,
//      style: TextStyle(
//          color: Colors.grey[600],
//      ),
//      underline: Container(
//        height: 2,
//        color: Colors.grey[600],
//      ),
      onChanged: (String newValue) {
        setState(() {
          _register.gender = newValue;
          _genderError = null;
        });
      },
      items: <String>['Select Gender', 'Male', 'Female']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  countyDropDown() {
    return DropdownButton<int>(
      hint: Text('Select County'),
      value: _register.county_id,
      onChanged: (int newValue) {
        setState(() {
          _register.county_id = newValue;
          _countyError = null;
        });
      },
      items: _counties.map((i) {
        return DropdownMenuItem<int>(
          value: i.id,
          child: Text(i.name),
        );
      }).toList(),
    );
  }

  industryDropDown() {
    return DropdownButton<int>(
      hint: Text('Select Industry'),
      value: _register.industry_id,
      onChanged: (int newValue) {
        setState(() {
          _register.industry_id = newValue;
          _industryError = null;
        });
      },
      items: _industries.map((i) {
        return DropdownMenuItem<int>(
          value: i.id,
          child: Text(i.name),
        );
      }).toList(),
    );
  }

  _validateForm() {
    bool _isValid = _formKey.currentState.validate();

    if (_register.gender == 'Select Gender') {
      setState(() => _genderError = "Please select your gender!");
      _isValid = false;
    }

    if (_register.industry_id == null) {
      setState(() => _industryError = "Please select your Industry!");
      _isValid = false;
    }

    if (_register.county_id == null) {
      setState(() => _countyError = "Please select your county!");
      _isValid = false;
    }

    _formKey.currentState.save();

    if (_isValid) {
      //form is valid
    }
  }
}
