import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/offers.dart';
import '../widgets/featured_event_item.dart';

class OffersPage extends StatelessWidget {
  static const String routeName = "/offers";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Provider.of<Offers>(context, listen: false).fetchOffers(),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (dataSnapshot.error != null) {
              print(dataSnapshot.error.toString());
              return Center(child: Text("Failed to load offers!"));
            } else {
              return Consumer<Offers>(
                builder: (ctx, eventData, child) => Padding(
                  padding: const EdgeInsets.all(8.0),
//                  child: GridView.builder(
//                      itemCount: eventData.items.length,
//                      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
//                        crossAxisCount: 2,
//                        childAspectRatio: 1 / 2,
//                        mainAxisSpacing: 4,
//                        crossAxisSpacing: 4
//                      ),
//                      itemBuilder: (ctx, i) {
//                        return FeaturedEventItem(event: eventData.items[i]);
//                      }),
                ),
              );
            }
          }
        });
  }
}
