import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/events.dart';
import '../widgets/featured_event_item.dart';

class EventsPage extends StatelessWidget {
  static const String routeName = "/events";

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Provider.of<Events>(context, listen: false).fetchEvents(),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (dataSnapshot.error != null) {
              print(dataSnapshot.error.toString());
              return Center(child: Text("Failed to load events!"));
            } else {
              return Consumer<Events>(
                builder: (ctx, eventData, child) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GridView.builder(
                      itemCount: eventData.items.length,
                      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 1 / 2,
                        mainAxisSpacing: 4,
                        crossAxisSpacing: 4
                      ),
                      itemBuilder: (ctx, i) {
                        return FeaturedEventItem(event: eventData.items[i]);
                      }),
                ),
              );
            }
          }
        });
  }
}
