import 'package:flutter/material.dart';
import 'package:paio/config/Palette.dart';
import 'package:paio/config/Styles.dart';
import 'package:paio/pages/events_page.dart';

class HomePage extends StatefulWidget {
  static const String routeName ="/home";
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{
  TabController _tabController;
  String title = "Paio";

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 5,vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body:TabBarView(
        controller: _tabController,
        children: <Widget>[
          Center(child: Text('Home')),
          EventsPage(),
          Center(child: Text('Offers')),
          Center(child: Text('News')),
          Center(child: Text('Account'))
        ],
      ),
      bottomNavigationBar: Material(
        elevation: 16,
        child: TabBar(
          controller: _tabController,
          labelColor:Palette.primaryColor,
          unselectedLabelColor: Palette.secondaryColor,
          indicatorColor:Palette.primaryColor,
          tabs: <Widget>[
            Tab(child: Text("Home",style: Styles.textMin), icon: Icon(Icons.home)),
            Tab(child: Text( "Events",style: Styles.textMin), icon: Icon(Icons.event)),
            Tab(child: Text( "Offers",style: Styles.textMin), icon: Icon(Icons.local_offer)),
            Tab(child: Text( "News",style: Styles.textMin), icon: Icon(Icons.featured_play_list)),
            Tab(child: Text( "Account",style: Styles.textMin), icon: Icon(Icons.person_outline)),
          ],
        )
      ),
    );
  }
}

