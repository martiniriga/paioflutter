import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:paio/pages/ForgotPassPage.dart';
import 'package:paio/pages/HomeTabPage.dart';
import 'package:paio/pages/RegisterPage.dart';
import 'package:provider/provider.dart';
import '../config/Palette.dart';
import '../config/Styles.dart';
import '../widgets/SubmitButton.dart';
import '../models/http_exception.dart';
import '../providers/auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _passwordFocusNode = FocusNode();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  var _isLoading = false;

  void _showErrorDialog(String message) {
    showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text("An Error occured!"),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(ctx).pop();
                  },
                )
              ],
            ));
  }

  @override
  void dispose() {
    _passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
            child: _isLoading
                ? Container(
                    height: 480,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Center(child: CircularProgressIndicator()),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text("Logging in ..."),
                        ),
                      ],
                    ),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.symmetric(vertical: 40),
                          child: Center(
                            child: Image.asset('assets/images/icon.png',
                                height: 120, width: 120),
                          )),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "LOGIN WITH",
                            style: Styles.textTiny,
                          ),
                          loginForm(),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 16, 8, 8),
                            child: Text("Forgot Password?",
                                style:
                                    TextStyle(color: Palette.secondaryColor)),
                          ),
                          GestureDetector(
                            onTap: () => showForgotPassword(),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 16, 0, 8),
                              child:
                                  Text("Retrieve", style: Styles.textHiglight),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 8, 0),
                            child: Text("Don't have an account?",
                                style:
                                    TextStyle(color: Palette.secondaryColor)),
                          ),
                          GestureDetector(
                            onTap: () => showRegisterPage(),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child:
                                  Text("Sign up", style: Styles.textHiglight),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 32)
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  loginForm() {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              controller: _emailController,
              decoration: const InputDecoration(
                hintText: 'Enter your email',
                labelText: 'Email',
              ),
              validator: (value) {
                bool emailValid =
                    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        .hasMatch(value);
                if (value.isEmpty) {
                  return 'Please enter email';
                } else if (!emailValid) {
                  return 'Please enter a valid email';
                }
                return null;
              },
              onFieldSubmitted: (_) {
                FocusScope.of(context).requestFocus(_passwordFocusNode);
              },
            ),
          ),
          TextFormField(
            controller: _passwordController,
            textInputAction: TextInputAction.done,
            decoration: const InputDecoration(
              hintText: 'Enter your password',
              labelText: 'Password',
            ),
            obscureText: true,
            autocorrect: false,
            focusNode: _passwordFocusNode,
            validator: (value) {
              if (value.isEmpty || value.length < 6) {
                return 'Password must be at least 6 characters';
              }
              return null;
            },
            onFieldSubmitted: (_) => _validateForm(),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: SubmitButton(
                      buttonText: "Login", selectHandler: _validateForm),
                ),
                Container(
                    margin: EdgeInsets.fromLTRB(16, 0, 0, 0),
                    child: OutlineButton(
                        child: Image.asset(
                          'assets/images/google.png',
                          height: 24,
                          width: 24,
                        ),
                        onPressed: () => showHomePage(),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)))),
              ],
            ),
          ),
        ],
      ),
    );
  }

  showForgotPassword() {
    Navigator.of(context).pushNamed(ForgotPassPage.routeName);
  }

  showRegisterPage() {
    Navigator.of(context).pushNamed(RegisterPage.routeName);
  }

  showHomePage() {
    Navigator.of(context).pushReplacementNamed(HomePage.routeName);
  }

  Future<void> _validateForm() async {
    bool _isValid = _formKey.currentState.validate();

    if (_isValid) {
      setState(() {
        _isLoading = true;
      });

      try {
        await Provider.of<Auth>(context, listen: false).login(
            _emailController.text.toString(),
            _passwordController.text.toString());
        showHomePage();
      } on HttpException catch (error) {
        var errorMessage = error.toString();
        _showErrorDialog(errorMessage);
      } catch (error) {
        const errorMessage = "Could not authenticate you.Please try again";
        _showErrorDialog(errorMessage);
      } finally {
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  void login() {
//    const url = 'https://paio.app/api/auth/login';
//    http
//        .post(
//      url,
//      headers: {"Content-Type": "application/json"},
//      body: json.encode({
//        'username': _emailController.text.toString(),
//        'password': _passwordController.text.toString()
//      }),
//    )
//        .then((response) {
//          print("${response.body}");
////      print(json.decode(response.body));
//      setState(() {
//        _isLoading = false;
//      });
////      showHomePage();
//    });
  }
}
