import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/http_exception.dart';

class Auth with ChangeNotifier {
  String _token;

  bool get isAuth {
    return token != null;
  }

  String get token {
    return _token;
  }

  Future<void> login(String email, String password) async {
    try {
      const url = "https://paio.app/api/auth/login";
      final response = await http.post(url,
          headers: {"Content-Type": "application/json"},
          body: json.encode({'username': email, 'password': password}));
      final responseData = json.decode(response.body);
      if (responseData['status'] == "error") {
        throw HttpException(responseData['message']);
      }
      _token = responseData['accessToken'];
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
