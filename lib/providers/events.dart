import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/event.dart';
import 'package:http/http.dart' as http;

class Events with ChangeNotifier {
  final String authToken;
  List<Event> _items = [];

  Events(this.authToken, this._items);

  List<Event> get items {
    return [..._items];
  }

  Event findById(int id) {
    return _items.firstWhere((ind) => ind.id == id);
  }

  Future<void> fetchEvents() async {
    const url = 'https://paio.app/api/events/all';
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $authToken'
    };
    try {
      final response = await http.get(url,headers: requestHeaders,);
      final result = json.decode(response.body);
      Iterable list = result['data'];
      if (list == null) {
        return;
      }
      print(list);
      _items = list.map((model) => Event.fromJson(model)).toList();
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
