import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/offer.dart';
import 'package:http/http.dart' as http;

class Offers with ChangeNotifier {
  final String authToken;
  List<Offer> _items = [];

  Offers(this.authToken, this._items);

  List<Offer> get items {
    return [..._items];
  }

  Offer findById(int id) {
    return _items.firstWhere((ind) => ind.id == id);
  }

  Future<void> fetchOffers() async {
    const url = 'https://paio.app/api/products/all';
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $authToken'
    };
    try {
      final response = await http.get(url,headers: requestHeaders,);
      final result = json.decode(response.body);
      Iterable list = result['data'];
      if (list == null) {
        return;
      }
      print(list);
      _items = list.map((model) => Offer.fromJson(model)).toList();
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
