import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/Industry.dart';
import 'package:http/http.dart' as http;


class Industries with ChangeNotifier {
  List<Industry> _items = [];

  List<Industry> get items {
    return [..._items];
  }

  Industry findById(int Id) {
    return _items.firstWhere((ind) => ind.id == Id);
  }


  Future<void> fetchIndustries() async {
    const url = 'https://paio.app/api/industries';
    try {
      final response = await http.get(url);
      final result = json.decode(response.body);
      Iterable list = result['data'];
      _items = list.map((model) => Industry.fromJson(model)).toList();
      notifyListeners();
      print(response);
    } catch (error) {
      throw error;
    }
  }
}
