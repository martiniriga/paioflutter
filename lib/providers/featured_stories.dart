import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/blog.dart';
import 'package:http/http.dart' as http;

class FeaturedStories with ChangeNotifier {
  final String authToken;
  List<Blog> _items = [];

  FeaturedStories(this.authToken, this._items);

  List<Blog> get items {
    return [..._items];
  }

  Blog findById(int id) {
    return _items.firstWhere((ind) => ind.id == id);
  }

  Future<void> fetchStories() async {
    const url = 'https://paio.app/api/blogs/featured';
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $authToken'
    };
    try {
      final response = await http.get(url,headers: requestHeaders,);
      final result = json.decode(response.body);
      Iterable list = result['data'];
      if (list == null) {
        return;
      }
      print(list);
      _items = list.map((model) => Blog.fromJson(model)).toList();
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
