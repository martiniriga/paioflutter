import 'package:flutter/material.dart';
import 'providers/events.dart';
import 'providers/auth.dart';
import 'providers/industries.dart';
import 'pages/events_page.dart';
import 'pages/profile_page.dart';
import 'pages/ForgotPassPage.dart';
import 'pages/LoginPage.dart';
import 'pages/RegisterPage.dart';
import 'pages/HomeTabPage.dart';
import 'config/Palette.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Industries(),
        ),
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        ChangeNotifierProxyProvider<Auth, Events>(
          builder: (ctx, auth, previousEvents) => Events(
            auth.token,
            previousEvents == null ? [] : previousEvents.items,
          ),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
            title: 'Paio',
            theme: ThemeData(
              primaryColor: Palette.primaryColor,
              fontFamily: 'Poppins',
            ),
            home: auth.isAuth ? HomePage() : LoginPage(),
            routes: {
              RegisterPage.routeName: (ctx) => RegisterPage(),
              ForgotPassPage.routeName: (ctx) => ForgotPassPage(),
              HomePage.routeName: (ctx) => HomePage(),
//              EventsPage.routeName: (ctx) => EventsPage(),
              ProfilePage.routeName: (ctx) => ProfilePage()
            }),
      ),
    );
  }
}
