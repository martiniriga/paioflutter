import 'package:flutter/material.dart';
import 'package:paio/models/Industry.dart';

class IndustryDropDown extends StatefulWidget {
  @override
  _IndustryDropDownState createState() => _IndustryDropDownState();
}

class _IndustryDropDownState extends State<IndustryDropDown> {
  final List<Industry> industries = [
    Industry(id: "1", name: "Art"),
    Industry(id: "2", name: "IT"),
    Industry(id: "3", name: "Manufacturing"),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        child: DropdownButton<String>(
      hint: Text('Select Industry'),
      value: _register.industry_id,
      onChanged: (String newValue) {
        setState(() {
          print(newValue);
          _register.industry_id = newValue;
          _industryError = null;
        });
      },
      items: industries.map((i) {
        return DropdownMenuItem<String>(
          value: i.id,
          child: Text(i.name),
        );
      }).toList(),
//      items: <String>['Select Industry','IT', 'Manufacturing']
//          .map<DropdownMenuItem<String>>((String value) {
//        return DropdownMenuItem<String>(
//          value: value,
//          child: Text(value),
//        );
//      }).toList(),
    ));
  }
}
