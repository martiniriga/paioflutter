import 'package:flutter/material.dart';

class EmailText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: TextFormField(
        decoration: const InputDecoration(
          hintText: 'Enter your email',
          labelText: 'Email',
        ),
        validator: (value) {
          bool emailValid =
          RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(value);
          if (value.isEmpty) {
            return 'Please enter email';
          } else if (!emailValid) {
            return 'Please enter a valid email';
          }
          return null;
        },
//        onSaved: (val) => setState(() => _register.email = val),
      ),
    );
  }
}
