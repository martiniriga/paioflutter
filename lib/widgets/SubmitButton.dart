import 'package:flutter/material.dart';
import '../config/Palette.dart';

class SubmitButton extends StatelessWidget {
  final String buttonText;
  final Function selectHandler;

  SubmitButton({this.buttonText, this.selectHandler});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: RaisedButton(
            color: Palette.accentColor,
            textColor: Palette.primaryTextColorLight,
            padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
            onPressed: () => selectHandler(),
            child: Text(buttonText),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8))));
  }
}
