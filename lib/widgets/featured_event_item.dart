import 'package:flutter/material.dart';
import '../config/Styles.dart';
import '../models/event.dart';
import 'package:intl/intl.dart';

class FeaturedEventItem extends StatelessWidget {
  final Event event;

  FeaturedEventItem({this.event});


  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            elevation: 0,
            child: Image.network(event.avatarUrl,
                width: double.infinity, height: 120, fit: BoxFit.cover)),
        Container(
//          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(DateFormat("d MMM y","en_US").format(DateTime.parse(event.startDate)),
              style: Styles.fontXs,textAlign: TextAlign.start,),
        ),
        Container(
//          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(
            event.name,
            style: TextStyle(fontSize: 15),
            maxLines: 1,
          ),
        ),
        Container(
//          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(
            event.excerpt,
            style: TextStyle(fontSize: 8.5),
            maxLines: 2,
          ),
        ),
        Row(
          children: <Widget>[
            Icon(
              Icons.pin_drop,
              color: Colors.red,
              size: 10,
            ),
            Container(
                margin: EdgeInsets.only(left:4),
                child: Text(event.location, style: Styles.fontXs)),
          ],
        ),
      ],
    );
  }
}
