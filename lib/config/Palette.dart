import 'package:flutter/material.dart';

// Color palette for the app
class Palette {
  static Color primaryColor = Colors.blue[900];
  static Color accentColor = Colors.yellow[700];
//    (0xff4fc3f7);
//  #FFC408
  static Color secondaryColor = Colors.grey[700];
  static Color grey6 = Colors.grey[600];
  static Color primaryTextColor = Colors.black;
  static Color primaryTextColorLight = Colors.white;
  static Color secondaryTextColor = Colors.black87;
  static Color secondaryTextColorLight = Colors.white70;
  static Color hintTextColor = Colors.black54;
  static Color hintTextColorLight = Colors.white70;
//  <color name="colorPrimary">#084261</color>
//  <color name="colorPrimaryDark">#084261</color>
//  <color name="colorAccent">#ffc408</color>
}