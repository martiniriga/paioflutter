import 'package:flutter/material.dart';

import 'Palette.dart';

class Styles {
  static TextStyle textHeading =
      TextStyle(color: Palette.primaryTextColor, fontSize: 20);
  static TextStyle textTiny =
      TextStyle(color: Palette.secondaryColor, fontSize: 10);
  static TextStyle textMin = TextStyle(fontSize: 9);
  static TextStyle fontXs = TextStyle(fontSize: 8.5, color: Palette.grey6);
  static TextStyle textHiglight = TextStyle(color: Palette.accentColor);
}
